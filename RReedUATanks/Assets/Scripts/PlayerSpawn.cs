﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    
    public GameObject[] sp; // this stores the spawn points into a array
    public GameObject Player; // will get the player object
    public GameObject TankPrefab; // will allow me to store the prefab
 
    // Start is called before the first frame update

    public GameObject Randomsp()
    {
        // calls the size i can store for spawnpoints
        return sp[UnityEngine.Random.Range(0, sp.Length)];
    }
  

    // Update is called once per frame
    void Update()
    {
        // if player gets destroyed respawn
        if (Player == null)
        {
            // will create the player at a random spawn point
            Player = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;


        }
    }
}
