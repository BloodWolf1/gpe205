﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TankMotor : MonoBehaviour
{
    
    // this holds the character controls component
    private CharacterController characterController;
    public TankData data; // allows me to call from tank data script
    public AIController ai;
        
    
    
    
   
   
   
    // Sart is called before the first frame update
    void Start()
    {
        // stores the scripts in a variable
        characterController = gameObject.GetComponent<CharacterController>();
        data = GetComponent<TankData>();
        ai = GetComponent<AIController>();
        

    }

    //this function will allow the tank to move forward
    public void Move(float speed)
    {
        //holds speed data
        Vector3 speedVector;
        // points object in same direction its facing
        speedVector = transform.forward;
        // apply the units speed value to move
        speedVector *= speed;
        //calls the function and sends it to the player controller and allows it to be meter per second.
        characterController.SimpleMove(speedVector);
    
    }
    //this function lets the tank go backwards
    public void Move2(float bspeed)
    {
        //holds speed data for going backwords
        Vector3 bSpeedVector;
        //lets tank go in reverse instead of forward by adding a negative
        bSpeedVector = -transform.forward;
        // apply the units speed value to move backwords
        bSpeedVector *= bspeed;
        //calls the function and sends it to the player controller and allows it to be meter per second.
        characterController.SimpleMove(bSpeedVector);
    }
    public void Rotate(float speed)
    {
        // creates a vector to hold the rotation info
        Vector3 rotateVector;
        // lets you rotate right and going left is a negative right
        rotateVector = Vector3.up; 
        // applies the rotation based on the speed
        rotateVector *= speed;
        // rotates the object by 1 meter converts roation to meter per second
        rotateVector *= Time.deltaTime;
        // rotates tank on this value by a local space
        transform.Rotate(rotateVector, Space.Self);
    }

    // this allows are enemy tanks to rotate towards a target. 

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;
        //vector to target gets differance for each position 
         vectorToTarget = target - ai.tf.position;
    
        // find quaternion that looks down the vector
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        // if we cant turn then it is false
            if (targetRotation == ai.tf.rotation)
        {
            return false;
        }
        else
        {
         // if we can use this to rotate the target rotation by are speed variable if we can turn
            ai.tf.rotation = Quaternion.RotateTowards(ai.tf.rotation, targetRotation, data.rSpeed * Time.deltaTime);
           
             return true;
        }
    
    }

}
