﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerUp  
{
    // allows me to add or subtract from current variables 
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifer;
    // the time the powerup is active
    public float duration;
    // checks to see if the powerup is permanently attached
    public bool isPermanent;


   
    public void OnActivate(TankData target)
    {
        target.fSpeed += speedModifier; // add the speed to the tank
        target.pHealth += healthModifier; // increas the health 
        target.maxHealth += maxHealthModifier; // will add to the max health the tank can have
        target.projectilespeed += fireRateModifer; // will increase the rate of fire

    }

    public void OnDeactivate(TankData target)
    {
        target.fSpeed -= speedModifier; // decrease the speed of tank
        target.pHealth -= healthModifier; // will lower the tank health
        target.maxHealth -= maxHealthModifier; // will lower the max health
        target.projectilespeed -= fireRateModifer; // will decrease the tank fire rate

    }

}
