﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public Transform[] waypoints;
    public float closeEnough = 1.0f;
    public TankMotor motor;
    public TankData data;
    private int currentWaypoint = 0;
   // public bool isPatrolForward = true;
    public Transform tf;
    public enum State { Stop, Loop, PingPong}
    public State state;

    private void Awake()
    {
        state = State.Stop;
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.rSpeed))
        {
            // Do nothing!
        }
        else
        {
            // Move forward
            motor.Move(data.fSpeed);
        }


        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {


            switch (state)
            {
                default:
                case State.Stop:
                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }

                    break;



                case State.Loop:

                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        //Otherwise go to waypoint 0
                        currentWaypoint = 0;
                    }
                    break;

                case State.PingPong:

                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        //Otherwise reverse direction and decrement our current waypoint
                       // isPatrolForward = false;
                        currentWaypoint--;
                    } 
            
                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        //Otherwise reverse direction and increment our current waypoint
                      //  isPatrolForward = true;
                        currentWaypoint++;
                    }
                    break;
                    
            }
        }
    }
}
