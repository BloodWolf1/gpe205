﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    // stores the tanks and spawn points into a array
    public GameObject[] spawnPoints;
    public GameObject[] EnemyTanks;


    public GameObject RandomEnemyPrefab()
    {
        // stores how many enemies
        return EnemyTanks[UnityEngine.Random.Range(0, EnemyTanks.Length)];
    }

    // Start is called before the first frame update
    void Start()
    {
        
        SpawnEnemy();
    }

    void SpawnEnemy()
    {
        // stores the length of spawn points
        int spawn = Random.Range(0, spawnPoints.Length);
      // creates the tanks 
         GameObject  EnemyTanks = Instantiate(RandomEnemyPrefab(), spawnPoints[spawn].transform.position, Quaternion.identity) as GameObject;
         // sets the parent 
        EnemyTanks.transform.parent = this.transform;
    }
  
}
