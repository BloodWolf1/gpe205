﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankHealth : MonoBehaviour
{
    public TankData data;
    public int bPoints; // amount of points you get for killing tank
    public Score scoring; // allows me to call function from Scoring script

    public void TakeDamage(int damageAmount)
    {
        // gets players health and subtracts damage taken
        data.ehealth -= damageAmount;
    

        if (data.ehealth <= 0)
        {
            // if destroyed add score to player hud
            Score.scoreValue += bPoints;
            Destroy(gameObject);
            print("Enemy has died");
        }


    }


}
