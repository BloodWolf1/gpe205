﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    // sotres the list of powerups I have
    public List<PowerUp> powerups;
    //calls the tank data
    public TankData data;

     void Start()
    {
        
        powerups = new List<PowerUp>();
    }
    // this function allows the powerup to be added to the object it is on 
    public void Add(PowerUp powerup)
    {
        // runs the powerup is its activated
        powerup.OnActivate(data);
        // if the powerup is permanent then add to list
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }
    void Update()
    {
        // shows a list of the expired powerups 
        List<PowerUp> expiredPoweruos = new List<PowerUp>();
        //this will loop through the list of powerups
        foreach(PowerUp power in powerups)
        {
            // a set timer to subtract the duration
            power.duration -= Time.deltaTime;

            // if the timer is 0 the powerup goes away and creates a list
            if(power.duration <= 0)
            {
                expiredPoweruos.Add(power);
            }
        }
        // if we looked at each powerup in the list we now look at the expired ones and remove them.
        foreach(PowerUp power in expiredPoweruos)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }

        // since the powerup is local we will remove it. 
        expiredPoweruos.Clear();
    }

}
