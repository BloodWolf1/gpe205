﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : MonoBehaviour
{
    
    public Score scoring; // allows me to call function from Scoring script
    public int health; // Enemy tanks health value
    public float speed; // speed of Enemy tank
    public float delayInSeconds; // delay in tank shots
    public Transform shotPoint; // casts bullet into the shotpoint get location
    private float shotTime; // calls the time for the bullet
    public float projectilespeed; // speed that the bullet goes aka force
    public GameObject enemyBullet; // calls the gameobject for bullet
    public int bPoints; // amount of points you get for killing tank
   
    // This function determines the tanks health and if below or reachs zero add score and destroy tank
    public void TakeDamage(int damageAmount)
    {
        // gets enemys health and subtracts damage taken
        health -= damageAmount;
        if (health <= 0)
        {
            // if destroyed add score to player hud
            Score.scoreValue += bPoints;
           
            Destroy(gameObject);
            print("Enemy has died");
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // this if statement calls time each shots is and creates the bullet 
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.time >= shotTime)
            {
                //creates the bullet and gets postion im casting from

                GameObject createbullet = Instantiate(enemyBullet, shotPoint.position, Quaternion.identity) as GameObject;
                //creats the phyics and control of bullet
                Rigidbody bulletrigidbody = createbullet.GetComponent<Rigidbody>();
                print("Enemy has fired");
                // add the speed of bullet and the direction it shoots
                bulletrigidbody.AddForce(gameObject.transform.forward * projectilespeed);
                //add time it takes to shoot bullet 
                shotTime = Time.time + delayInSeconds;
            }
        }
    }
}
