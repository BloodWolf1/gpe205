﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public TankData TankData;
    // Lets me put multiple tanks in a enemy list
    public List<AIController> enemies;
    // allows me to have multple players in a list.
    public List<InputController> players;

    //runs before start function
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("ERROR: There can be only one Game Manager.");
            Destroy(gameObject);
        }
        enemies = new List<AIController>();
        players = new List<InputController>();


    }

    
    public void EnemySpawn()
    {

    }

    void Update()
    {
        
       
    }

}
