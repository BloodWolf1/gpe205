﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PauseMenu : MonoBehaviour
{
    // game wont be pused until button pressed
    public static bool GameIsPaused = false;
    // gets the gameobject for ui
    public GameObject pauseMenuUi;

    void Update()
    {
        // if i press the p key do an option
        if (Input.GetKeyDown(KeyCode.P))
        {
            // pause game
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

   public void Resume()
    {
        // if game is paused can't do anything
        pauseMenuUi.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        // if game is active un pause game
        pauseMenuUi.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void QuitGame()
    {
        //  quit game
        Application.Quit();
    }

}
