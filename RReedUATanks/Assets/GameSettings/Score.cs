﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{
    
    public static int scoreValue; // gets current score
    public static int highScore; // gets the highscore
    public Text score; // applies the text to the screen
    public Text highscore; // ui text for high score
    public Text livesreamining; // ui text for lives

    public TestSpawn spawn; // calls the functions from spawner script
  

    void Start()
    {
        // calls the highscore in text
      
        highscore.text = PlayerPrefs.GetInt("highScore", 0).ToString();
        
    }

    public void Scoreall()
    {
        // gets the score value and creates text
        score.text = scoreValue.ToString();
       // checks if current score is greater then highscore if so set new highscore
        if (scoreValue > PlayerPrefs.GetInt("highScore", 0))
        {
            PlayerPrefs.SetInt("highScore", scoreValue);
            highscore.text = scoreValue.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
       
        Scoreall();
        Reset();

    }
   public static void AddScore(int pointsadded)
    {
        // adds the points togther
        scoreValue += pointsadded;
    }
    public void Reset()
    {
        // if i press the z key reset highscore 
        if (Input.GetKeyDown(KeyCode.Z))
        {
            PlayerPrefs.DeleteAll();
            highscore.text = "0";
        }
            
    }


}
