﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCamera : MonoBehaviour
{
    private Camera cam2;

    // Start is called before the first frame update
    void Start()
    {
        cam2 = GameObject.Find("Cam2").GetComponent<Camera>();
        if (cam2)
        {
            cam2.rect = new Rect(0.5f, 0, 0.5f, 1);
            Camera.main.rect = new Rect(0, 0, 0.5f, 1);
        }
        else
        {
            Camera.main.rect = new Rect(0, 0, 1, 1);
        }
    }
}
