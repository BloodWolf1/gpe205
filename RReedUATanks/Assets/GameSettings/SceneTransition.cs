﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;





public class SceneTransition : MonoBehaviour
{
public void LoadScene(string sceneName)
    {
        // goes to the next scene I enter by name
        StartCoroutine(Transition(sceneName));
    }

    IEnumerator Transition(string SceneName)
    {
        // waits 1 second to load 
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneName);
    }


}
