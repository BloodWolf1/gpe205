﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandMine : MonoBehaviour
{
    public GameObject explosion;
    public GameObject Sound;
    public int damage; // the damage it deals to the player


    public void OnTriggerEnter(Collider col)
    {// if it connects to the player tag it will react.
        if (col.tag == "Player")
        {
            // when it goes off play a sound
            Instantiate(Sound, transform.position, transform.rotation);
            // gets the player tank health component and deals damage. 
            col.GetComponent<TankHealth>().TakeDamage(damage);
           
            Destroy(gameObject);
        }
        
    }


}
