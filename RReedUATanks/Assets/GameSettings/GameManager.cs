﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public TankData TankData;
    public MapGen2 map;
    // Lets me put multiple tanks in a enemy list
    public List<StateManager> enemies;

    public GameObject playerPrefab;
    // allows me to have multple players in a list.
    private List<InputController> player;
    public enum GameState { RandomMapstate, Mapofthedaystate}
     public GameState game = GameState.Mapofthedaystate;

    //runs before start function
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("ERROR: There can be only one Game Manager.");
            Destroy(gameObject);
        }
        enemies = new List<StateManager>();
     


    }
    public void Player()
    {
        player = new List<InputController>();
       // AddPlayer(new InputController { })
    }

    public void AddPlayer(InputController inputController)
    {
        player.Add(inputController);
    }

    public void ChangeState(GameState newState)
    {
        game = newState;
        //stateEnterTime = Time.time;
    }
    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour
            + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
    public Button startGame;
    public void Randommapo()
    {
         
    Debug.Log("button pressed");
        startGame.onClick.AddListener(delegate
        {
            map.RandomGrid();

            // PlayerPrefs.Save();
        });
        
    }

    public void Random()
    {
       
      map.mapSeed = DateToInt(DateTime.Now);

    }


    public void Map()
    {
        map.mapSeed = DateToInt(DateTime.Now.Date);
    }

}
