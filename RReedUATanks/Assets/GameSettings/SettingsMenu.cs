﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    // gets audio mixers
    public AudioMixer audioMixer;
    public AudioMixer audioMixer2;

    // lets me set the game audio to a value
    public void SetVolumeGame (float volume)
    {

        audioMixer.SetFloat("Volume", volume);
    }

    // lets me set the music audio to a value
    public void SetVolumeMusic(float volume1)
    {
        
        audioMixer2.SetFloat("Music", volume1);
        

    }

    public void QuitGame()
    {
        // quit game
        Application.Quit();
    }
}
