﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // call the target location
    public Transform target;
    // allows me to postion the camera where i want
    public Vector3 offset;

    void Update()
    {
        // this will allow the camera to be updated on the target and controls
        FindPlayer();
        transform.LookAt(target);
        transform.position = target.position + offset;
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }
    // this will find the gameobject with the tag player on it and attach it self to it
    void FindPlayer()
    {
        // finds the gameobject
        GameObject findPlayer;
        GameObject findPlayer2;
        // places the object with the tags
        findPlayer = GameObject.FindGameObjectWithTag("Player");
        findPlayer2 = GameObject.FindGameObjectWithTag("Player2");

        target = findPlayer.transform;
        target = findPlayer2.transform;
    }

}
