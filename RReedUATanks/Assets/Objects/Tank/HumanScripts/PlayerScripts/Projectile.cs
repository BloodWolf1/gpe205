﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public TankData data;
    public float bulletLifetime; // the lifespan of the bullet
    public int damage; // the damage the bullet does
    public GameObject bulletsound; // the bullet sound when fired
    public GameObject hitobjectSound; // the sound when it hit something

    void Start()
    {
       Destroyprojectile();
    }
  // destroy the projectile when it hits something or lifetime goes away. 
    public void Destroyprojectile()
    {
        Destroy(gameObject,  bulletLifetime);
        Instantiate(bulletsound, transform.position, transform.rotation);
        
    }
    // this function takes care of the collion the projectile colides with
    private void OnTriggerEnter(Collider collision)
    { 
        
        // if the bullet hits the enemy tank, it takes damage and destroys the bullet on contact
      if(collision.tag == "Enemy")
        {

            Instantiate(hitobjectSound, transform.position, transform.rotation);
            collision.GetComponent<EnemyTankHealth>().TakeDamage(damage);
           
            Destroy(gameObject);
        }
      // this checks to see if this hits a wall if so destroy
      if(collision.tag == "Ground")
        {
            Instantiate(hitobjectSound, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }


}
 