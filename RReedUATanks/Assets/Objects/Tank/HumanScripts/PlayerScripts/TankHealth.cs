﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    public TankData data;
    // makes slider public to I can adjust the health
    public Slider healthBar;

    public void Start()
    {
        // Finds the SLider object and links it to tank
        healthBar = FindObjectOfType<Slider>();
        // gets the max healh of the tank and sets it to slider
        healthBar.maxValue = data.maxHealth;
        // gets the current health and sets to slider
        healthBar.value = data.pHealth;
        
       
    }
    public void TakeDamage(int damageAmount)
    {
        // gets players health and subtracts damage taken
        data.pHealth -= damageAmount;
        // gets the players health
        healthBar.value = data.pHealth;
        // if my health hits zero i die
        if (data.pHealth <= 0)
        {
            Instantiate(data.deathSound, transform.position, transform.rotation);
            Destroy(gameObject);
            print("Player has died");
        }

    }
    
     

    

}
