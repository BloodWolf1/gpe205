﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public TankMotor motor; // this allows me to call things from tankmotor
    public TankData data; // this allows me to call componets from Tank data
    public enum InputScheme { WASD, IJKL }; // calls the move inputs i can use
    public InputScheme input = InputScheme.WASD; // I chose to use WASD

    void Update()
    {
        // Lets me press these buttons and go in there direction
        switch (input)
        {
            case InputScheme.WASD:
                // lets me move forward
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.fSpeed);

                }
                //lets me move backwards
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move2(data.bSpeed);



                }
                // allows me to rotate right
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Rotate(data.rSpeed);
                }
                //allows me to rotate left
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Rotate(-data.rSpeed);

                    //another way to rotate 


                }
                break;
        }
        // Input for 2nd player
        switch (input)
        {
            case InputScheme.IJKL:
                // lets me move forward
                if (Input.GetKey(KeyCode.I))
                {
                    motor.Move(data.fSpeed);

                }
                //lets me move backwards
                if (Input.GetKey(KeyCode.K))
                {
                    motor.Move2(data.bSpeed);



                }
                // allows me to rotate right
                if (Input.GetKey(KeyCode.L))
                {
                    motor.Rotate(data.rSpeed);
                }
                //allows me to rotate left
                if (Input.GetKey(KeyCode.J))
                {
                    motor.Rotate(-data.rSpeed);

                    //another way to rotate 


                }
                break;
        }
    }

} 

