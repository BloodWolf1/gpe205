﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWeapon : MonoBehaviour
{
    public TankData data; // allows me to call from the tankdata script
    public GameObject bullet; // creates and calls bullet
    public Transform shotPoint; // casts bullet into the shotpoint get locations its facing
    private float shotTime; // calls the time for the bullet

    // Update is called once per frame
    void Update()
    {
        //if i press space key shoot bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // this if statement calls time each shots is and creates the bullet
            if (Time.time >= shotTime)
            {
                //creates the bullet and gets postion its casting from. the location of shotpoint
                GameObject createbullet = Instantiate(bullet, shotPoint.position, Quaternion.identity) as GameObject;

                //creats the physics and control of bullet
                Rigidbody bulletrigidbody = createbullet.GetComponent<Rigidbody>();
                print("Player has fired");
                // add the speed of bullet and the direction it shoots
               bulletrigidbody.AddForce(gameObject.transform.forward * data.projectilespeed);
                //add time it takes to shoot bullet 
                shotTime = Time.time + data.delayInSeconds;
            }
        }
       
    }
}
