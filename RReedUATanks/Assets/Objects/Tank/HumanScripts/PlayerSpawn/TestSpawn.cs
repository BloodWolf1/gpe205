﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TestSpawn : MonoBehaviour
{
    public GameObject[] sp; // this stores the spawn points into a array
    public GameObject Player1; // will get player1 object
    public GameObject Player2; // will get the player2 object
    public GameObject TankPrefab; // will allow me to store the prefab
    public GameObject TankPrefab2; // player 2 prefab
    public SceneTransition sceneTransition; // allows me to trantion from a scene
    public int playerlives; // number of lives i cant set
   

    // public PlayerLives lives;

    bool player1 = true;
    bool player2 = true;
    public GameObject Randomsp()
    {
        // calls the size i can store for spawnpoints
        return sp[UnityEngine.Random.Range(0, sp.Length)];
    }

    public void Twoplayer()
    {
        // creats two players. 
        Player1 = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;
        Player2 = Instantiate(TankPrefab2, Randomsp().transform.position, Quaternion.identity) as GameObject;
    }
    public void OnePlayer()
    {
        // create player 1 
        Player1 = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(player1 == true)
        {
           // OnePlayer();
        }
        if (player2 == true)
        {
            Twoplayer();
        }

    }

    // Update is called once per frame
    void Update()
    {
       
        

        // if player1 is null and looses lives respawn. 
        if (Player1 == null && playerlives >= 1)
        {
          
            // subtract a life
            playerlives--;
          
            // will create the player at a random spawn point
            Player1 = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;
            // if player is at zero health its game over. 
            if (playerlives <= 0)
            {
                sceneTransition.LoadScene("GameOver");
            }

        }
        if (Player2 == null && playerlives >= 1)
        {
            playerlives--;

            // will create the player at a random spawn point
            Player2 = Instantiate(TankPrefab2, Randomsp().transform.position, Quaternion.identity) as GameObject;

            if (playerlives >= 0)
            {
               sceneTransition.LoadScene("GameOver");
            }
        }
    }
}
