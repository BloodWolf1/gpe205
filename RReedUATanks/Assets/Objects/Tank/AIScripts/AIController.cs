﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public Transform[] waypoints; // allows me to store multiple tranforms into a array
    public TankData data;
    public TankMotor motor;
    public Transform tf; 
    public Transform target; // gets the target
    
   
    public float closeEnough = 1.0f; // checks to see if the waypoint is close
    private int avoidenceStage = 0; // the stage the tank is in to avoid
    public float avoidencetime =2f; // the amount of time the tank will take to avoid
    private float exitTime; // the time the the tank  stays in a state
    public float fleeDistance = 1.0f; // the distance the tank will flee
    private int currentWaypoint = 0; // calls the current point the tank is going


    public enum AIState { Chase, Hear, Flee, Navagate, } //this allows me to call differnt states the tank is in
    public AIState aiState; // current state the tank is in default
    public float StateEnterTime; // the time the tank changes it's state
    public float aiSenseRadius; // the radius from sound
 

    void Awake()
    {
        // the scripts that will get stored in a variable 
        tf = gameObject.GetComponent<Transform>();
        motor = GetComponent<TankMotor>();
        data = GetComponent<TankData>();
    }
    void FindPlayer()
    {
        GameObject findPlayer;
        findPlayer = GameObject.FindGameObjectWithTag("Player");
        target = findPlayer.transform;
    }
    public void ChangeState(AIState newState)
    {
        // allows me to change state
        aiState = newState;

        //save the time we changed states
        StateEnterTime = Time.time;
    }

    void DoChase()
    {
        // this function allows the enemy to chase the player
        motor.RotateTowards(target.position, data.rSpeed);
        if (CanMove(data.fSpeed))
        {

            motor.Move(data.fSpeed);
        }
        else
        {
            avoidenceStage = 1;
        }
    }
    // this will have the tank flee from the player
    void DoFlee()
    {
        // gets the target postion and subract from the enemy tank
        Vector3 vectorToTarget = target.position - tf.position;

        // allows the tank to turn away from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // normalize the vector to 1
        vectorAwayFromTarget.Normalize();

        // changes the vector to a length 
        vectorAwayFromTarget *= fleeDistance;

        // We can find the position we want to move 
        //This allows the tank vector to turn away from our current position.
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.rSpeed);
        motor.Move(data.fSpeed);
        if (CanMove(data.fSpeed))
        {
            motor.Move(data.fSpeed);
        }
        else
        {
            avoidenceStage = 1;
        }

    }
    // this will allow the tank to move if it hits the player
    bool CanMove(float speed)
    {
 
        RaycastHit hit;
        if 
            (Physics.Raycast(tf.position, tf.forward, out hit, speed + 1))
        {
          
            if (!hit.collider.CompareTag("Player"))
            {
                return false;

            }
        }
             return true;
 
    }

     public bool CanSee(TankData otherTank)
    {
        // handle FOV
        // find the vector to the other object
        Vector3 vectorToOtherTank = otherTank.transform.position - transform.position;

       // find the angle between those 2 vectors
        float viewAngle = Vector3.Angle(vectorToOtherTank, transform.forward);
      //  if the angle is > our feild of view then its out of vison and cant see it
        if (viewAngle > data.fieldOfView)
        {
            return false;
        }
        // Handle line of sight


        /// raycast (mathmatically calulate a line from tank to other tank by view distance)
        RaycastHit hit;
        // first object that ray hits is other tank it can see it
        if (Physics.Raycast(transform.position, vectorToOtherTank, out hit, data.viewDistance))
        {
            //  get the tankdata of thing i hit
            TankData viewObjectData = hit.collider.gameObject.GetComponent<TankData>();
            // if that is object im looing for
            if (viewObjectData == otherTank)
            {
                //  i saw it
                return true;

            }

            // this function allows the enemy to chase the player
            motor.RotateTowards(target.position, data.rSpeed);
            if (CanMove(data.fSpeed))
            {

                motor.Move(data.fSpeed);
            }
            else
            {
                avoidenceStage = 1;
            }

        }

        // otherwiswe our line of sight is blocked cant see tank or nothing in range
        return false;
    }
    public bool CanHear(TankData otherTank)
    {
        //get the noisemaking ablility on other tank
        NoiseMaker otherNoise = otherTank.gameObject.GetComponent<NoiseMaker>();
        //if it doesnt exist then we cant hear it
        if (otherNoise == null)
        {
            return false;
        }
        else
        {
            // if it does exist, check if closer then volume * hearingmodifier
            if (Vector3.Distance(otherTank.transform.position, transform.position)
                <= otherNoise.soundVolume * data.hearingModifier)
            {
                return true;
            }
        }

        // if i get hear and havnt heard it yet, then cant hear
        return false;
    }

    // this will allow the tanks to avoid obsitcaled depending on what stage it is in
    void DoAvoidance()
    {

        if (avoidenceStage == 1)
        {
            motor.Rotate(-1 * data.rSpeed);
         



            if (CanMove(data.fSpeed))
            {
                avoidenceStage = 2;
                exitTime = avoidencetime;
            }
        }

        else if (avoidenceStage == 2)
        {
            if (CanMove(data.fSpeed))
            {
                exitTime -= Time.deltaTime;
                motor.Move(data.fSpeed);

                if (exitTime <= 0)
                {
                    avoidenceStage = 0;
                }
            }

            else
            {
                avoidenceStage = 1;
            }

        }

    }
    void Update()
    {
        
        // this will allow the tanks to move in the state they are in
        //chase state will follow the player
         if (aiState == AIState.Chase)
         {
          
            if (avoidenceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
              
            }


        }
         // the flee state will run away from the player
        else if (aiState == AIState.Flee)
        {
            
            if (avoidenceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }


        }
        else if (aiState == AIState.Hear)
        {

            if (avoidenceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                
            }


        }

        // this state will allow the tank to move by waypoints and follow a route
        else if (motor.RotateTowards(waypoints[currentWaypoint].position, data.rSpeed))
        {
            
            // Do nothing!
        }
        else
        {
            // Move forward
            motor.Move(data.fSpeed);
        }

        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {

            if (aiState == AIState.Navagate)
            {

                // Advance to the next waypoint, if we are still in range
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    // goes back to waypoint 0
                    currentWaypoint = 0;
                   
                }
                if (avoidenceStage != 0)
                {
                    DoAvoidance();
                }
            }
        }
         FindPlayer();
    }
   
}



