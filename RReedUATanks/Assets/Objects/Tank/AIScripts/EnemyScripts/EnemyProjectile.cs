﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float bulletLifetime; // the lifespan of the bullet
    public int damage; // the damage the bullet does
    public GameObject bulletsound; // sound of the gun fireing
    public GameObject hitobjectSound; // sound of the bullet hitting something
    public TankData data;
    

    void Awake()
    {
        data = GetComponent<TankData>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(bulletsound, transform.position, transform.rotation);
        Destroy(gameObject, bulletLifetime);
    }
 
    // this function takes care of the collion the projectile colides with
    private void OnTriggerEnter(Collider collision)
    {
        // if the bullet hits the player tank, it takes damage and destroys the bullet on contact
        if (collision.tag == "Player")
        {
            Instantiate(hitobjectSound, transform.position, transform.rotation);
            collision.GetComponent<TankHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }
        // this checks to see if this hits a wall if so destroy
        if (collision.tag == "Ground")
        {
            Instantiate(hitobjectSound, transform.position, transform.rotation);
            Destroy(gameObject);
        }

    }
}
