﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankHealth : MonoBehaviour
{
    public TankData data;
    public int bPoints; // amount of points you get for killing tank
    public Score scoring; // allows me to call function from Scoring script
    public SceneTransition sceneTransition;

    void Awake()
    {
        data = GetComponent<TankData>();
        scoring = GetComponent<Score>();
        //sceneTransition = GetComponent<SceneTransition>();
    }
    public void TakeDamage(int damageAmount)
    {
      
        // gets players health and subtracts damage taken
        data.ehealth -= damageAmount;
       
       // if (data.maxHealth <= data.ehealth)
       // {
          //  data.ehealth = data.maxHealth;
            
            if (data.ehealth<= 0)
            {
           
             // if destroyed add score to player hud
                 Score.scoreValue += bPoints;
                Instantiate(data.deathSound, transform.position, transform.rotation);
                Destroy(gameObject);
                print("Enemy has died");
                

            }

       // }
    }


}
