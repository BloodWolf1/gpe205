﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankWeapon : MonoBehaviour
{
    public StateManager manage;
    public TankData data; // allows me to call from the tankdata script
    public Transform shotPoint; // casts bullet into the shotpoint get locations its facing
    private float shotTime; // calls the time for the bullet
    public GameObject enemyBullet; // calls the gameobject for bullet

     void Awake()
    {
        data = GetComponent<TankData>();

    }

    void Update()
    {
        // this if statement calls time each shots is and creates the bullet 
            if (Time.time > shotTime)
            {

                //creates the bullet and gets postion im casting from

                GameObject createbullet = Instantiate(enemyBullet, shotPoint.position, Quaternion.identity) as GameObject;
                //creats the phyics and control of bullet
                Rigidbody bulletrigidbody = createbullet.GetComponent<Rigidbody>();
                print("Enemy has fired");
                // add the speed of bullet and the direction it shoots
                bulletrigidbody.AddForce(gameObject.transform.forward * data.projectilespeed);
                //add time it takes to shoot bullet 
                shotTime = Time.time + data.delayInSeconds;

            }
         
    }
}
