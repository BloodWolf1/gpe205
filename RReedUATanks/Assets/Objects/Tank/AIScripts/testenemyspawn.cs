﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testenemyspawn : MonoBehaviour
{
    public GameObject[] sp;
    public GameObject[] EnemyTanks;

    // Start is called before the first frame update

    public GameObject Randomsp()
    {
        return sp[UnityEngine.Random.Range(0, sp.Length)];
    }

    public GameObject RandomEnemyPrefab()
    {
        return EnemyTanks[UnityEngine.Random.Range(0, EnemyTanks.Length)];
    }


    // Update is called once per frame
    void Update()
    {
        if (EnemyTanks == null)
        {

          GameObject  EnemyTanks = Instantiate(RandomEnemyPrefab(), Randomsp().transform.position, Quaternion.identity) as GameObject;
          EnemyTanks.transform.parent = this.transform;

        }
    }

}
