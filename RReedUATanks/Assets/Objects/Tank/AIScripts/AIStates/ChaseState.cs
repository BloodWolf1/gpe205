﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;
    // state the tank is in
    public enum AIChaseState { Chase, Flee};
    public AIChaseState aichaseState = AIChaseState.Chase;

    public float stateEnterTime;
    public float aiSenseRadius;

   void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
    }
    // allows me to change what state the tank is in
    public void ChangeState(AIChaseState newState)
    {
        aichaseState = newState;
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // if its in chase state avoid obsticales and chase player
        if (aichaseState == AIChaseState.Chase)
        {

            if (stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                stateManager.DoChase();
            }
            // if half health change to flee state
            if (data.ehealth < data.maxHealth * 0.5f)
            {

                ChangeState(AIChaseState.Flee);

            }
        }
        // if in flee state run from player
        else if (aichaseState == AIChaseState.Flee)
        {

            if (stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {

                stateManager.DoFlee();

            }
        }

        
    }
   
}
