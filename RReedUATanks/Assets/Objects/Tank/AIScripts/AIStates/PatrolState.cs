﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;
    public TankMotor motor;
    // state the tank is in
    public enum AIPatrolState { Patrol};
    public AIPatrolState aipatrolstate = AIPatrolState.Patrol;
    public float stateEnterTime;
    public float aiSenseRadius;

    void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
        motor = GetComponent<TankMotor>();
    }
    // allows me to change what state the tank is in
    public void ChangeState(AIPatrolState newState)
    {
        aipatrolstate = newState;
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (motor.RotateTowards(stateManager.waypoints[stateManager.currentWaypoint].position, data.rSpeed))
        {

            // Do nothing!
        }
        else
        {
            // Move forward
            motor.Move(data.fSpeed);
        }

        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(stateManager.waypoints[stateManager.currentWaypoint].position - stateManager.tf.position) < (stateManager.closeEnough * stateManager.closeEnough))
        {

            if (aipatrolstate == AIPatrolState.Patrol)
            {

                // Advance to the next waypoint, if we are still in range
                if (stateManager.currentWaypoint < stateManager.waypoints.Length - 1)
                {
                    stateManager.currentWaypoint++;
                }
                else
                {
                    // goes back to waypoint 0
                    stateManager.currentWaypoint = 0;

                }
                // avoid obsticales to reach waypoints
                if (stateManager.avoidenceStage != 0)
                {
                    stateManager.DoAvoidance();
                }
            }
        } 
     
    }
    
}
