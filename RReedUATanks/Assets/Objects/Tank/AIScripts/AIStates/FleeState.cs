﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;
    // state the tank is in
    public enum AIfleeState {Cansee}
    public AIfleeState aIflee = AIfleeState.Cansee;

    public float stateEnterTime;
    public float aiSenseRadius;

    void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
     
    }
    // allows me to change what state the tank is in
    public void ChangeState(AIfleeState newState)
    {
        aIflee = newState;
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // if it can see the player run away and avoid obsticales
        if (aIflee == AIfleeState.Cansee)
        {


            if (stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                if (stateManager.CanSee())
                {
                    
                    stateManager.DoFlee();
                }


            }
        }
    }
}
