﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SIghtState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;


    public enum AISightState {Hear}
    public AISightState aISight = AISightState.Hear;
    public float stateEnterTime;
    public float aiSenseRadius;


    void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
    }
    public void ChangeState(AISightState newState)
    {
        aISight = newState;
        stateEnterTime = Time.time;
    }

    void Update()
    {
        // if the enemy can hear the player chase after him
        if (aISight == AISightState.Hear)
        {
            if (stateManager.CanHear())
            {
                stateManager.DoChase();
                //avoid obsticales while chasing
                if (stateManager.avoidenceStage != 0)
                {
                    stateManager.DoAvoidance();
                }
            } 
        }
    }

}
