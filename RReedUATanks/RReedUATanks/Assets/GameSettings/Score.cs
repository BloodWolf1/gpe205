﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{
    // allows the scorevalue to change from other scripts
    public static int scoreValue = 0;
    Text score; // applies the text to the screen
    // how many lives the player has
    public Text lives;

   public PlayerSpawn playerSpawn;
    void Start()
    {
        // calls the tesxt and score
        score = GetComponent<Text>();
        lives = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // update the text based on the scorevalue
        score.text = "Score:" + scoreValue;
        //lives.text = "Lives Remaining" + playerSpawn.playerlives;

    }

}
