﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
   public Text text;
 public void Save()
    {
        PlayerPrefs.SetString("TextData", text.text);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        text.text = PlayerPrefs.GetString("TextData");
    }

}
