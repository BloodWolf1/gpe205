﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class ScoreData : IComparable<ScoreData>
{
    public int score = 0;
    public string name;
    public int HighScore = 0;

    List<ScoreData> scores;
    
    
    public int CompareTo(ScoreData other)
    { 
        scores.Sort();
        
        if (other == null)
        {
            
            return 1;
        }
        if (this.score > other.score)
        {
            return 1;
        }
        if (this.score < other.score)
        {
            return -1;
        }
        
        scores.Reverse();
        scores = scores.GetRange(0, 3);

        return 0;

        
    }
   
  
   
}
