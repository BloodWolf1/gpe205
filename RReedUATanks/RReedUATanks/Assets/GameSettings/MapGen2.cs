﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class MapGen2 : MonoBehaviour
{
    public int rows; //creates the rows made for our generator
    public int cols; // how many column we create
    public float roomWidth; // the width of each room made
    public float roomheight; // the height of each room/ walls 
    public GameObject[] gridPrefabs; // the list of room prefabs i have made
    private Room[,] grid;

    public int mapSeed;

    
    bool RandomMap = true;
    bool mapoftheday = true;
    // spawns in a random room 
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public void GenerateGrid()
    {
            UnityEngine.Random.InitState(mapSeed);
   
            // clears the grid and will allow to create a new one
            grid = new Room[rows, cols];
            // for each grid row made
            for (int currentrows = 0; currentrows < rows; currentrows++)
            { // each column in the row is made
                for (int currentcols = 0; currentcols < cols; currentcols++)
                {
                    // the location of each room goes
                    float xPostion = roomWidth * currentcols;
                    float zPostion = roomheight * currentrows;
                    Vector3 newPostion = new Vector3(xPostion, 0.0f, zPostion);
                    // creates the gird at a good location.
                    GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPostion, Quaternion.identity) as GameObject;
                    // the parent of the object
                    tempRoomObj.transform.parent = this.transform;
                    //gives a name of the object
                    tempRoomObj.name = "Room_" + currentcols + "," + currentrows;
                    // gets the room object
                    Room tempRoom = tempRoomObj.GetComponent<Room>();
                    // will open the doors if it alligns with the other rooms 
                    if (currentrows == 0)
                    {
                        // if door is bottom row open door
                        tempRoom.doorNorth.SetActive(false);
                    }
                    else if (currentrows == rows - 1)
                    {
                        //if door is top row open door
                        tempRoom.doorSouth.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorNorth.SetActive(false);
                        tempRoom.doorSouth.SetActive(false);
                    }
                    if (currentcols == 0)
                    {
                        //if we are on the frist column open door
                        tempRoom.doorEast.SetActive(false);
                    }
                    else if (currentcols == cols - 1)
                    {
                        // if door is on last column open door
                        tempRoom.doorWest.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorEast.SetActive(false);
                        tempRoom.doorWest.SetActive(false);
                    }
                    // will save the grid created to the array
                    grid[currentcols, currentrows] = tempRoom;
                }
            }
    }
    // this will create a random map
    public void RandomGrid()
    {
            mapSeed = DateToInt(DateTime.Now);
            GenerateGrid();
        
    }
    //this will create the map of the day. 
    public void MapofDay()
    {
        GenerateGrid();
        mapSeed = DateToInt(DateTime.Now.Date);
    }

    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour
            + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    // Start is called before the first frame update
    void Start()
    {
        // if the map is true create grid
        if (RandomMap == true)
        {
            RandomGrid();
        }
        else 
        {
            RandomMap = false;
        }
        
        // if the map is true create grid
         if (mapoftheday == true)
        {
            MapofDay();
        }
        else
        {
            mapoftheday = false;
        }
      
        
       
    }


    void Update()
    {
        UnityEngine.Random.InitState(DateToInt(DateTime.Now));
       

    }

}


