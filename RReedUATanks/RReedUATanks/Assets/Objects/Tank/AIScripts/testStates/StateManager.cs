﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public Transform[] waypoints; // allows me to store multiple tranforms into a array
    public TankData data;
    public TankMotor motor;
    public Transform tf;
    public Transform target; // gets the target


    public float closeEnough = 1.0f; // checks to see if the waypoint is close
    public int avoidenceStage = 0; // the stage the tank is in to avoid
    public float avoidencetime = 2f; // the amount of time the tank will take to avoid
    private float exitTime; // the time the the tank  stays in a state
    public float fleeDistance = 1.0f; // the distance the tank will flee
    public int currentWaypoint = 0; // calls the current point the tank is going

    void Awake()
    {
        // the scripts that will get stored in a variable 
        tf = gameObject.GetComponent<Transform>();
        motor = GetComponent<TankMotor>();
        data = GetComponent<TankData>();
        
    }
    void FindPlayer()
    {
        GameObject findPlayer;
        findPlayer = GameObject.FindGameObjectWithTag("Player");
        target = findPlayer.transform;
    }
    public void DoChase()
    {
        // this function allows the enemy to chase the player
        motor.RotateTowards(target.position, data.rSpeed);
        if (CanMove(data.fSpeed))
        {

            motor.Move(data.fSpeed);
        }
        else
        {
            avoidenceStage = 1;
        }
    }
    public void DoFlee()
    {
        // gets the target postion and subract from the enemy tank
        Vector3 vectorToTarget = target.position - tf.position;

        // allows the tank to turn away from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // normalize the vector to 1
        vectorAwayFromTarget.Normalize();

        // changes the vector to a length 
        vectorAwayFromTarget *= fleeDistance;

        // We can find the position we want to move 
        //This allows the tank vector to turn away from our current position.
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.rSpeed);
        motor.Move(data.fSpeed);
        if (CanMove(data.fSpeed))
        {
            motor.Move(data.fSpeed);
        }
        else
        {
            avoidenceStage = 1;
        }

    }

    bool CanMove(float speed)
    {

        RaycastHit hit;
        if
            (Physics.Raycast(tf.position, tf.forward, out hit, speed + 1))
        {

            if (!hit.collider.CompareTag("Player"))
            {
                return false;

            }
        }
        return true;

    }

    public void DoAvoidance()
    {

        if (avoidenceStage == 1)
        {
            motor.Rotate(-1 * data.rSpeed);




            if (CanMove(data.fSpeed))
            {
                avoidenceStage = 2;
                exitTime = avoidencetime;
            }
        }

        else if (avoidenceStage == 2)
        {
            if (CanMove(data.fSpeed))
            {
                exitTime -= Time.deltaTime;
                motor.Move(data.fSpeed);

                if (exitTime <= 0)
                {
                    avoidenceStage = 0;
                }
            }

            else
            {
                avoidenceStage = 1;
            }

        }

    }

    public bool CanSee(TankData otherTank)
    {
        // handle FOV
        // find the vector to the other object
        Vector3 vectorToOtherTank = otherTank.transform.position - transform.position;

        // find the angle between those 2 vectors
        float viewAngle = Vector3.Angle(vectorToOtherTank, transform.forward);
        //  if the angle is > our feild of view then its out of vison and cant see it
        if (viewAngle > data.fieldOfView)
        {
            return false;
        }
        // Handle line of sight


        /// raycast (mathmatically calulate a line from tank to other tank by view distance)
        RaycastHit hit;
        // first object that ray hits is other tank it can see it
        if (Physics.Raycast(transform.position, vectorToOtherTank, out hit, data.viewDistance))
        {
            //  get the tankdata of thing i hit
            TankData viewObjectData = hit.collider.gameObject.GetComponent<TankData>();
            // if that is object im looing for
            if (viewObjectData == otherTank)
            {
                //  i saw it
                return true;
                
            }

            // this function allows the enemy to chase the player
            motor.RotateTowards(target.position, data.rSpeed);
            if (CanMove(data.fSpeed))
            {

                motor.Move(data.fSpeed);
            }
            else
            {
                avoidenceStage = 1;
            }

        }

        // otherwiswe our line of sight is blocked cant see tank or nothing in range
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        FindPlayer();
    }

    

}
