﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SIghtState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;


    public enum AISightState { See,StopShoot,Hear}
    public AISightState aISight = AISightState.See;
    public float stateEnterTime;
    public float aiSenseRadius;


    void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
    }
    public void ChangeState(AISightState newState)
    {
        aISight = newState;
        stateEnterTime = Time.time;
    }

    void Update()
    {
        if (aISight == AISightState.See)
        {
            if (stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                //stateManager.CanSee();
            }
          
            if (Vector3.Distance(stateManager.target.position, stateManager.tf.position) <= aiSenseRadius)
            {
                ChangeState(AISightState.StopShoot);
            }
        }
        else if (aISight == AISightState.StopShoot)
        {

           
        }

    }











}
