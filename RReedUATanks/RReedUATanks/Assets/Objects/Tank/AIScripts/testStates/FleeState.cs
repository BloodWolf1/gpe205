﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;

    public enum AIfleeState { Cansee, Flee}
    public AIfleeState aIflee = AIfleeState.Cansee;
    public float stateEnterTime;
    public float aiSenseRadius;

    void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
    }

    public void ChangeState(AIfleeState newState)
    {
        aIflee = newState;
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (aIflee == AIfleeState.Cansee)
        {
            if (stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                //stateManager.CanSee();
            }

            if (Vector3.Distance(stateManager.target.position, stateManager.tf.position) <= aiSenseRadius)
            {
                ChangeState(AIfleeState.Flee);
            }
        } else if(aIflee == AIfleeState.Flee)
        {
            stateManager.DoFlee();
        }
    }
}
