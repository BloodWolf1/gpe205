﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : MonoBehaviour
{
    public StateManager stateManager;
    public TankData data;

    public enum AIChaseState { Chase, Fire, Flee};
    public AIChaseState aichaseState = AIChaseState.Chase;
    public float stateEnterTime;
    public float aiSenseRadius;

   void Awake()
    {
        data = GetComponent<TankData>();
        stateManager = GetComponent<StateManager>();
    }

    public void ChangeState(AIChaseState newState)
    {
        aichaseState = newState;
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
         
        if (aichaseState == AIChaseState.Chase)
        {
            if(stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                stateManager.DoChase();
            }
            if(data.ehealth < data.maxHealth * 0.5f)
            {
                ChangeState(AIChaseState.Flee);
            } else if (Vector3.Distance (stateManager.target.position, stateManager.tf.position) <= aiSenseRadius)
            {
                ChangeState(AIChaseState.Fire);
            }
           
        } else if( aichaseState == AIChaseState.Flee)
        {
            if(stateManager.avoidenceStage != 0)
            {
                stateManager.DoAvoidance();
            }
            else
            {
                stateManager.DoFlee();
            }
        }
        
    }
   
}
