﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSpawn : MonoBehaviour
{
    
    public GameObject[] sp; // this stores the spawn points into a array
    public GameObject Player; // will get the player object
    public GameObject TankPrefab; // will allow me to store the prefab
    public GameObject TankPrefab2; // will allow me to store the prefab


    public SceneTransition sceneTransition;
    public int playerlives;
    public Text livesremaing;

    // Start is called before the first frame update

    public GameObject Randomsp()
    {
        // calls the size i can store for spawnpoints
        return sp[UnityEngine.Random.Range(0, sp.Length)];
    } 
    void Start()
    {
       // livesremaing = GetComponent<Text>();
        twoplayer();
        

    }
    public void twoplayer()
    {
    Player = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;
    Player = Instantiate(TankPrefab2, Randomsp().transform.position, Quaternion.identity) as GameObject;
    }
    // Update is called once per frame
    void Update()
    {
        livesremaing.text = "Live Remaining" + playerlives;
        // if player gets destroyed respawn
        if (Player == null && playerlives >=1)
        {
            playerlives--;
          
        // will create the player at a random spawn point
         Player = Instantiate(TankPrefab, Randomsp().transform.position, Quaternion.identity) as GameObject;
  
            if (playerlives >= 0)
            {
                sceneTransition.LoadScene("GameOver");
            }

        }
    }
}
