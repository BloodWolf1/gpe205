﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public PowerUp powerUp;
    public AudioClip feedback;


    public GameObject[] pickupPrefab;
    public float spawnDelay;
    private float nextSpawnTime;
    private Transform tf;
    public GameObject spawnedPickup;
    public GameObject pickupSound;


    public GameObject RandomPickupPrefab()
    {
        return pickupPrefab[UnityEngine.Random.Range(0, pickupPrefab.Length)];
    }
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
    }

    public void OnTriggerEnter(Collider other)
    {
        // stores the variables if another object has the powerupcontroller script
        PowerUpController powCon = other.GetComponent<PowerUpController>();

        // if the other object has the script add the powerup
        if(powCon != null)
        {
            powCon.Add(powerUp);

            // play sound if it is picked up
            if(feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f);
            }
            Instantiate(pickupSound, transform.position, transform.rotation);
            Destroy(gameObject);

            
        }

    }

     void Update()
    {
        // if the pickup is there dont spawn another one
        if (spawnedPickup == null)
        {
            // will spawn a pickup if nothings there.
            if (Time.time > nextSpawnTime)
            {
                // will create the pickup and wait to spawn another one
               GameObject spawnedPickup = Instantiate(RandomPickupPrefab(), tf.position, Quaternion.identity) as GameObject;
                spawnedPickup.transform.parent = transform;
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            // check to see if it still exists if so wait to spawn again
            nextSpawnTime = Time.time + spawnDelay;
        }
    }






}
