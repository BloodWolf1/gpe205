﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TankData : MonoBehaviour
{
    //Data
    public float fSpeed; // is tanks speed, meter per second
    public float rSpeed; //is tanks roation, degrees per second
    public float bSpeed; // the speed of the backwords movement
    public float pHealth; // player current Health
    public float ehealth; // enemy health
    public float maxHealth; // player max health
    public float delayInSeconds; // Time between each shot
    public float projectilespeed; // speed of bullet or force
    public float hearingModifier = 1.0f; // allows the enemy tank to hear
    public float fieldOfView = 45f; // the cone vison the can see 
    public float viewDistance = 10f; // the distance it can see 
    // Allows me to call sounds on both player tanks and enemy
    public GameObject deathSound;

    // components
    public TankMotor motor;


    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<TankMotor>();

    }

}




