﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour
{
    
    public int rows; //creates the rows made for our generator
    public int cols; // how many column we create
    public float roomWidth; // the width of each room made
    public float roomheight; // the height of each room/ walls 
    public GameObject[] gridPrefabs; // the list of room prefabs i have made
    private Room[,] grid;

    public int mapSeed;
    
    // true/false for which game mode to play.
    public bool isMapOfTheDay;
    public bool RandomMap;
    public bool Seed;

   // spawns in a random room 
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public void GenerateGrid()
    {
        // if the bool is selected go with this option 
        if (Seed)
        {
            UnityEngine.Random.InitState(mapSeed);
            // clears the grid and will allow to create a new one
            grid = new Room[rows, cols];
            // for each grid row made
            for (int i = 0; i < rows; i++)
            { // each column in the row is made
                for (int j = 0; j < cols; j++)
                {
                    // the location of each room goes
                    float xPostion = roomWidth * j;
                    float zPostion = roomheight * i;
                    Vector3 newPostion = new Vector3(xPostion, 0.0f, zPostion);
                    // creates the gird at a good location.
                    GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPostion, Quaternion.identity) as GameObject;
                    // the parent of the object
                    tempRoomObj.transform.parent = this.transform;
                    //gives a name of the object
                    tempRoomObj.name = "Room_" + j + "," + i;
                    // gets the room object
                    Room tempRoom = tempRoomObj.GetComponent<Room>();
                    // will open the doors if it alligns with the other rooms 
                    if (i == 0)
                    {
                        // if door is bottom row open door
                        tempRoom.doorNorth.SetActive(false);
                    }
                    else if (i == rows - 1)
                    {
                        //if door is top row open door
                        tempRoom.doorSouth.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorNorth.SetActive(false);
                        tempRoom.doorSouth.SetActive(false);
                    }
                    if (j == 0)
                    {
                        //if we are on the frist column open door
                        tempRoom.doorEast.SetActive(false);
                    }
                    else if (j == cols - 1)
                    {
                        // if door is on last column open door
                        tempRoom.doorWest.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorEast.SetActive(false);
                        tempRoom.doorWest.SetActive(false);
                    }
                    // will save the grid created to the array
                    grid[j, i] = tempRoom;
                }
            }
        }
        else if (RandomMap)
        {

            // clears the grid and will allow to create a new one
            grid = new Room[rows, cols];
            // for each grid row made
            for (int i = 0; i < rows; i++)
            { // each column in the row is made
                for (int j = 0; j < cols; j++)
                {
                    // the location of each room goes
                    float xPostion = roomWidth * j;
                    float zPostion = roomheight * i;
                    Vector3 newPostion = new Vector3(xPostion, 0.0f, zPostion);
                    // creates the gird at a good location.
                    GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPostion, Quaternion.identity) as GameObject;
                    // the parent of the object
                    tempRoomObj.transform.parent = this.transform;
                    //gives a name of the object
                    tempRoomObj.name = "Room_" + j + "," + i;
                    // gets the room object
                    Room tempRoom = tempRoomObj.GetComponent<Room>();
                    // will open the doors if it alligns with the other rooms 
                    if (i == 0)
                    {
                        // if door is bottom row open door
                        tempRoom.doorNorth.SetActive(false);
                    }
                    else if (i == rows - 1)
                    {
                        //if door is top row open door
                        tempRoom.doorSouth.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorNorth.SetActive(false);
                        tempRoom.doorSouth.SetActive(false);
                    }
                    if (j == 0)
                    {
                        //if we are on the frist column open door
                        tempRoom.doorEast.SetActive(false);
                    }
                    else if (j == cols - 1)
                    {
                        // if door is on last column open door
                        tempRoom.doorWest.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorEast.SetActive(false);
                        tempRoom.doorWest.SetActive(false);
                    }
                    // will save the grid created to the array
                    grid[j, i] = tempRoom;
                }
            }

        }
        else if (isMapOfTheDay)
        {
            UnityEngine.Random.InitState(mapSeed);

            // clears the grid and will allow to create a new one
            grid = new Room[rows, cols];
            // for each grid row made
            for (int i = 0; i < rows; i++)
            { // each column in the row is made
                for (int j = 0; j < cols; j++)
                {
                    // the location of each room goes
                    float xPostion = roomWidth * j;
                    float zPostion = roomheight * i;
                    Vector3 newPostion = new Vector3(xPostion, 0.0f, zPostion);
                    // creates the gird at a good location.
                    GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPostion, Quaternion.identity) as GameObject;
                    // the parent of the object
                    tempRoomObj.transform.parent = this.transform;
                    //gives a name of the object
                    tempRoomObj.name = "Room_" + j + "," + i;
                    // gets the room object
                    Room tempRoom = tempRoomObj.GetComponent<Room>();
                    // will open the doors if it alligns with the other rooms 
                    if (i == 0)
                    {
                        // if door is bottom row open door
                        tempRoom.doorNorth.SetActive(false);
                    }
                    else if (i == rows - 1)
                    {
                        //if door is top row open door
                        tempRoom.doorSouth.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorNorth.SetActive(false);
                        tempRoom.doorSouth.SetActive(false);
                    }
                    if (j == 0)
                    {
                        //if we are on the frist column open door
                        tempRoom.doorEast.SetActive(false);
                    }
                    else if (j == cols - 1)
                    {
                        // if door is on last column open door
                        tempRoom.doorWest.SetActive(false);
                    }
                    else
                    {
                        // if the doors are in the middle open both
                        tempRoom.doorEast.SetActive(false);
                        tempRoom.doorWest.SetActive(false);
                    }
                    // will save the grid created to the array
                    grid[j, i] = tempRoom;
                }
            }
        }

    }

    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour 
            + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    // Start is called before the first frame update
    void Start()
    {
        // will initialize the grid
        GenerateGrid();
        if (isMapOfTheDay)
        {
            mapSeed = DateToInt(DateTime.Now.Date);
        }

    }
    void Update()
    {
        UnityEngine.Random.InitState(DateToInt(DateTime.Now));
       
    }

}
